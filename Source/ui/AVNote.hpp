//
//  AVNote.hpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 12/18/16.
//
//

#ifndef AVNote_hpp
#define AVNote_hpp


/**
the AVNote class deals with taking the mouse position and generating the visual and audio synthesis
 */


#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"


class AVNote : public Timer, public Component
{
public:
    
    /** Constructor */
    AVNote(Audio *a, Component *c, int x, int y);
    
    /** Destructor */
    ~AVNote();
    
    /** draws rect */
    void paint(Graphics &g) override;
    
    /** increases opacity of drawn rect with each callback */
    void timerCallback() override;
  
    /** converts click coordinates to single int 
     @param x is the x coordinate
     @param y is the y coordinate*/
    void playNote (int x, int y);

    
private:
    Component *parentComponent;
    float opacity;
    int x, y;
    Audio *audio;
    
};

#endif /* AVNote_hpp */
