//
//  PadGui.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 05/12/2016.
//
//

#ifndef PadGui_hpp
#define PadGui_hpp

#include "../../JuceLibraryCode/JuceHeader.h"
#include "AVNote.hpp"

/**
 Class creates the musical surface that the user clicks to generate a note.
 */



class PadGui :  public Component
{
public:
    /** Constructor */
    PadGui (Audio& a);
    
    /** Destructor */
    ~PadGui();
    
    /** Paint function creates the light blue suface */
    void paint (Graphics &g) override;

    /** takes the coordinates of click */
    void mouseDown (const MouseEvent& event) override;
    

    
private:
    
    int x, y;
    float opacity;
    
    Audio *a;

};

#endif /* PadGui_hpp */
