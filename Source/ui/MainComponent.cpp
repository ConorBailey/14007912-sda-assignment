/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a), padGui (a)
{
    setSize (1280, 800);
    

    
    // setup for waveform selection comboBox
    waveform.setText("Choose a Waveform");
    waveform.addItem("Sine", 1);
    waveform.addItem("Square", 2);
    waveform.addItem("Saw", 3);
    addAndMakeVisible(waveform);
    waveform.addListener(this);
   
    audio.setScale(1);

    
    // setup for the scale selection comboBox
    scale.setText("Choose a Scale");
    scale.addItem("Major", 1);
    scale.addItem("Minor", 2);
    scale.addItem("Blues", 3);
    scale.addItem("Mixolydian", 4);
    addAndMakeVisible(scale);
    scale.addListener(this);

    
    
    // adds the musical surface gui to the application
    addAndMakeVisible(padGui);
    
    // label for delay gain rotary control
    delGainLabel.setText("Delay Gain", NotificationType::dontSendNotification);
    addAndMakeVisible(delGainLabel);
    
    //label for filter cutoff
    filterLabel.setText ("Filter Cutoff", NotificationType::dontSendNotification);
    addAndMakeVisible(filterLabel);
    
    // delay time label
    delayTimeLabel.setText("Delay Time", dontSendNotification);
    addAndMakeVisible(delayTimeLabel);

    // creates a rotary control for delay feedback gain
    delGain.setRange(0.05, 0.95);
    delGain.setValue(0.5);
    delGain.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    addAndMakeVisible(delGain);
    delGain.addListener(this);
    
    //creates rotary control for filter cutoff
    filterCutoff.setRange(500, 20000);
    filterCutoff.setValue(2000);
    filterCutoff.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    addAndMakeVisible(filterCutoff);
    filterCutoff.addListener(this);
    
    //creates rotary control that sets delay time
    delayTime.setRange(10000, 40000);
    delayTime.setValue(20000);
    delayTime.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    addAndMakeVisible(delayTime);
    delayTime.addListener(this);
    
   

}

MainComponent::~MainComponent()
{
}

void MainComponent::resized()
{
    
    // bounds of UI components are set
    waveform.setBounds(760, 50, getWidth()/4 -10, 30);
    scale.setBounds(760, 95, getWidth()/4 -10, 30);
    padGui.setBounds(50, 50, 700, 700);
    delGain.setBounds(760, 200, 100, 100);
    delGainLabel.setBounds(760, 220, 100, 100);
    filterCutoff.setBounds(880, 200, 100, 100);
    filterLabel.setBounds(880, 220, 100, 100);
    delayTime.setBounds(1000, 200, 100, 100);
    delayTimeLabel.setBounds(1000, 220, 100, 100);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::darkgrey, true);
        }
    }
}





void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    
    
    if (comboBoxThatHasChanged == &waveform)
    {
        audio.setWaveform(waveform.getSelectedId());
        
    }
    
    else if (comboBoxThatHasChanged == &scale)
    {
        audio.setScale(scale.getSelectedId());
    }
 
}

void MainComponent::sliderValueChanged (Slider* sliderThatHasChanged)
{
    if (sliderThatHasChanged == &delGain)
    {
        audio.setDelGain(delGain.getValue());
        
    }
    
    else if (sliderThatHasChanged == &filterCutoff)
    {
        audio.setFilterCutoff(filterCutoff.getValue());
    }
    
    else if (sliderThatHasChanged == &delayTime)
    {
        audio.getDelay()->setDelayTime(delayTime.getValue());
    }
}
