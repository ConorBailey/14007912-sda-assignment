//
//  PadGui.cpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 05/12/2016.
//
//

#include "PadGui.hpp"



PadGui::PadGui (Audio& a)
{
    
    this->a = &a;
    
    x = 0;
    y = 0;
}

PadGui::~PadGui()
{
    
}


void PadGui::paint (Graphics &g)
{
    g.fillAll(Colours::powderblue);
}

void PadGui::mouseDown (const MouseEvent& event)
{
    //mouse coordinates read into appropriate variables
    x = event.x;
    y = event.y;
    
    //avnote is sent the mouse coordinates
    AVNote *note = new AVNote(a, this, x, y);
    
    //bounds for drawn rect are set here
    note->setBounds(x-15, y-15, 30, 30);
    addAndMakeVisible(note);
    
}
