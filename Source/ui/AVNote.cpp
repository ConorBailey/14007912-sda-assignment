//
//  AVNote.cpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 12/18/16.
//
//

#include "AVNote.hpp"


AVNote::AVNote(Audio *a, Component *c, int x, int y)
{
    
    opacity = 1.f;
    
    //the x and y class variables are set to the values passed into the function
    this->x = x;
    this->y = y;
    
    
    this->parentComponent = c;
    this->audio = a;
    
    startTimer(50);
    
    playNote(x, y);

}

void AVNote::paint (Graphics &g)
{
    g.fillAll(Colours::powderblue);
    g.setOpacity (opacity * opacity);
    g.drawRect(0, 0, getWidth(), getHeight());
}


AVNote::~AVNote()
{

}

void AVNote::playNote (int x, int y)
{
    
    const int numOfColumns = 4;
    const int numOfRows = 3;
    
    // calculation returns number of pixels per column
    int pixPerCol = parentComponent->getWidth() / numOfColumns;
    // calculation returns the column the mouse was clicked in
    int mouseColumn = x / pixPerCol;
    
    int pixPerRow = parentComponent->getHeight() / numOfRows;
    int mouseRow = y / pixPerRow;
    
    // calculation takes sum of column and row multiplied by num of columns to give position in grid
    int mousePosition = mouseColumn + (mouseRow * numOfColumns);
    audio->setNote(mousePosition);
    
    DBG (mousePosition);
    
}


void AVNote::timerCallback()
{
    //each timer callback makes note more opaque
    opacity -= 0.05;
    
    if (opacity <= 0.f){
        stopTimer();
        parentComponent->removeChildComponent(this);
        delete this;
        
        return;
    }
    
    parentComponent->repaint();
    
}

