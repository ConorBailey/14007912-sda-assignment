/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "PadGui.hpp"
#include "Delay.hpp"




//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public ComboBox::Listener,
                        public Slider::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

    /** 
     listener function for comboboxes
     */
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged)override;
    
    /** 
     listener function for sliders 
     */
    void sliderValueChanged (Slider* sliderThatHasChanged) override;
    
private:
    Audio& audio;
    TextButton ctrlButton;
    ComboBox waveform;
    PadGui padGui;
    Slider delGain;
    Label delGainLabel;
    ComboBox scale;
    Slider filterCutoff;
    Label filterLabel;
    Slider delayTime;
    Label delayTimeLabel;



    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
