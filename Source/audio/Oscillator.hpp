//
//  Oscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 11/11/2016.
//
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for an oscillator
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     SinOscillator constructor
     */
    Oscillator();
    
    /**
     SinOscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     @param freq receives a float point number which is used to set the frequency
     
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     @param noteNum recieves a note number to convert to use to set the note
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     @param amp takes a float value and uses it to set amplitude
     */
    void setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     @param sr takes float value and uses it to set sample rate
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     @return next sample of oscillation is returned in the form of a floating point value
     */
    float nextSample();
    
    /**
     function that provides the execution of the waveshape
     */
    virtual float renderWaveShape (const float currentPhase) = 0;
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};


#endif /* Oscillator_hpp */
