//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#include "SawOscillator.hpp"

SawOscillator::SawOscillator()
{
    reset();
}

SawOscillator::~SawOscillator()
{
    
}

float SawOscillator::renderWaveShape(float currentPhase)
{
    return (currentPhase * 2) - 1;
}
