//
//  SawOscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#ifndef SawOscillator_hpp
#define SawOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

/**
 Class for a sawtooth wave oscillator, @see Oscillator
 */

class SawOscillator : public Oscillator
{
public:
    /** Constructor */
    SawOscillator();
    
    /** Destructor */
    virtual ~SawOscillator();
    
    /**
     function that provides the execution of the waveshape
     @param currentPhase takes the current phase of the oscillator and uses this as a basis to render the wave
     @return rendered waveshape as a saw wave
     */
    float renderWaveShape (const float currentPhase) override;
    
    
};

#endif /* SawOscillator_hpp */
