//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 11/11/2016.
//
//

#include "SquareOscillator.hpp"

SquareOscillator::SquareOscillator()
{
    reset();
}

SquareOscillator::~SquareOscillator()
{
    
}


float SquareOscillator::renderWaveShape (const float currentPhase)
{
    if (currentPhase <= M_PI)
    {
        return 1;
    }
    else
    {
        return -1;
    }

}
