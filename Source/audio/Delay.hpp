//
//  Delay.hpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#ifndef Delay_hpp
#define Delay_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Delay
{
public:
    
    /** Constructor */
    Delay();
    
    /** Destructor */
    ~Delay();
    
    /** generates a delayed signal
     @param newSignal takes a floating point input
     @return is a delayed version of the input*/
    float delayTapOut(float newSignal);
    
    /** sets delay time
     @param newDelayTime takes floating point number and uses it to set delay time*/
    void setDelayTime (float newDelayTime);
    
    float interpolatedRead (float readPos);
    

    
private:
    float *circularBuffer;
    float delayTime;
    int bufferSize, bufferWritePos;
    
    LagrangeInterpolator larangeInterpolator;
};

#endif /* Delay_hpp */
