//
//  TriOscillator.cpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#include "TriOscillator.hpp"
#include <cmath>



TriOscillator::TriOscillator()
{
    reset();
}

TriOscillator::~TriOscillator()
{
    
}

float TriOscillator::renderWaveShape (float currentPhase)
{
    return 1-(fabs(currentPhase * 0.5) * 4);
}
