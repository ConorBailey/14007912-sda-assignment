//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor Bailey on 11/11/2016.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "Oscillator.hpp"

/**
 Class for a square wave oscillator, @see Oscillator
 */

class SquareOscillator : public Oscillator
{
    
public:

    /** Constructor */
    SquareOscillator();
    
    /** Destructor */
    ~SquareOscillator();
    
    /**
     function that provides the execution of the waveshape
     @param currentPhase takes the current phase of the oscillator and uses this as a basis to render the wave
     @return rendered waveshape as a square wave
     */
    float renderWaveShape (const float currentPhase) override;

    
private:
    
    
};


#endif //H_SQUAREOSCILLATOR
