/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SawOscillator.hpp"
#include "SquareOscillator.hpp"
#include "Oscillator.hpp"
#include "Delay.hpp"




class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback, public Component, public Timer, public IIRFilter
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
  
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    

    /** sets the waveform type
     @param newWaveform takes integer and uses as an index to select the waveform type*/
    void setWaveform(const int newWaveform);
    
    /** sets the note number
     @param position takes integer and uses as an index to select note from aray
     @see AVNote::playnote */
    void setNote (const int position);
    
    /** sets the gain of the delay
     @param newDelGain takes a floating point value and uses this to scale the amplitude of the delays gain*/
    void setDelGain (const float newDelGain);
    
    /** sets the scale of choice
     @param newScale takes an integer and uses this as an index to select an array of note nums
     @see setNote notemap arrays first dimension*/
    void setScale (const int newScale);
    
    /** each callback decreases the synths output */
    void timerCallback() override;
    
    /** sets filter cutoff
     @param newCutoff takes a floating point value and uses this to set the cutoff of the delay*/
    void setFilterCutoff (const float newCutoff);
    
    Delay* getDelay() {return &delay1;}

private:
    
    
    AudioDeviceManager audioDeviceManager;
    MidiMessage outputMsg;
    
    Atomic <Oscillator*> oscPtr;
    SquareOscillator squareOscillator;
    SinOscillator sinOscillator;
    SawOscillator sawOscillator;
    IIRFilter LPF;
    
    Delay delay1;
    
    float sr;
    float delGain;
    float ampEnv;
    int scaleSelection;
};



#endif  // AUDIO_H_INCLUDED
