//
//  TriOscillator.hpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#ifndef TriOscillator_hpp
#define TriOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

class TriOscillator : public Oscillator
{
public:
    TriOscillator();
    ~TriOscillator();
    
    virtual float renderWaveShape (const float currentPhase) override;
};


#endif /* TriOscillator_hpp */
