/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    sr = 44100;

    /** points the oscillator to the sinOscillator class */
    oscPtr = &sinOscillator;
    
    ampEnv = 1.f;
    startTimer(1);
    scaleSelection = 1;
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    oscPtr.get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
    oscPtr.get()->setAmplitude(message.getVelocity() / 127.f);
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];

    float delaySig;
    float filterSig;
    
    while(numSamples--)
    {
        
        
        
        float synthOut = oscPtr.get()->nextSample() * (ampEnv * ampEnv);
        
        filterSig = LPF.processSingleSampleRaw(synthOut);

        delaySig = delay1.delayTapOut(filterSig);
        
        
        *outL = filterSig + delaySig;
        *outR = filterSig + delaySig;
        

        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
//    oscPtr.get()->setSampleRate(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}



void Audio::setWaveform(int newWaveform)
{
    if (newWaveform == 1)
    {
        oscPtr = &sinOscillator;
        
    }

    
    else if (newWaveform == 2)
    {
        oscPtr = &squareOscillator;
        
    }

    else if (newWaveform == 3)
    {
        oscPtr = &sawOscillator;
        
    }
    

}

void Audio::setNote(int position)
{
    /* mouse position turns to note number here. available notes are changed by the scale combobox **/
    const int noteMap[5][12] = {
                    {60, 62, 64, 65, 67, 69, 60, 72, 73, 76, 77, 79},
                    {60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79},
                    {60, 62, 63, 65, 67, 68, 70, 72, 74, 75, 77, 79},
                    {60, 63, 65, 66, 67, 70, 72, 72, 75, 77, 78, 79},
                    {60, 62, 64, 65, 67, 69, 60, 72, 73, 76, 77, 79}
                    
                };
    

    
    const int noteNum = noteMap[scaleSelection][position];
    
    
    if (noteNum >= 0)
    {
        outputMsg = MidiMessage::noteOn (1, noteNum, (uint8) 100);
        ampEnv = 1.f;
        handleIncomingMidiMessage (nullptr, outputMsg);
        
    }
    
    
}


void Audio::setDelGain(float newDelGain)
{
    delGain = newDelGain;
}

void Audio::setScale(int newScale)
{
    if (newScale >= 1)
    {
        scaleSelection = newScale;
        
    }
}

void Audio::timerCallback()
{
    ampEnv -= 0.001;
    if (ampEnv < 0.f)
    {
        ampEnv = 0.f;
        
    }
    

}

void Audio::setFilterCutoff(float newCutoff)
{
    LPF.setCoefficients(IIRCoefficients::makeLowPass(sr, newCutoff));
    
}
