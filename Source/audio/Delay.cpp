//
//  Delay.cpp
//  JuceBasicAudio
//
//  Created by Conor George Bailey on 1/16/17.
//
//

#include "Delay.hpp"

Delay::Delay()
{
    
    int sampleRate = 44100;

    // initialises the circular buffer
    int pos=0;
    bufferSize = (int) (2.0*sampleRate);
    circularBuffer = new float[bufferSize];
    
    for(pos = 0; pos < bufferSize; pos++)
    {
        circularBuffer[pos] = 0;

    }
    
    bufferWritePos = 0;
    delayTime = 20000;

    
}

Delay::~Delay()
{
    
}


float Delay::delayTapOut(float newSignal)
{

    int bufferReadPos;
    
    //increment the buffer
    bufferWritePos++;
    //wraps around to 0 at max buffer size
    if (bufferWritePos == bufferSize)
        bufferWritePos = 0;
    
    //sets read position at the write position minus delay time
    bufferReadPos = (bufferWritePos - delayTime);
    //adds buffer size t read position
    if (bufferReadPos < 0)
        bufferReadPos += bufferSize;
    
    float delSig = interpolatedRead(bufferReadPos);
    float delSigInterp;
    
    larangeInterpolator.process(1, &delSig, &delSigInterp, 1);

    
    //writes the signal to buffer
    circularBuffer[bufferWritePos] = newSignal + delSigInterp * 0.5f;
    
    
    return delSig;
}


void Delay::setDelayTime(float newDelayTime)
{
    delayTime = newDelayTime;
}

float Delay::interpolatedRead(float readPos)
{
    int pos1, pos2;
    float pdiff, vdiff, result;
    
    vdiff = 0;
    
    pos1 = (int) readPos;
    pos2 = pos1 + 1;
    if (pos2 == bufferSize)
        pos2 = 0;
    
    pdiff = readPos - pos1;
    vdiff = circularBuffer[pos2] - circularBuffer[pos1];
    result = circularBuffer[pos1] + (vdiff * pdiff);
    
    return result;
    
    
}
