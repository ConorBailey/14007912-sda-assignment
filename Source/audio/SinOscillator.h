/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"


/**
 Class for a sinewave oscillator @see Oscillator
 */

class SinOscillator : public Oscillator
{
public:
	//==============================================================================
	/**
	 SinOscillator constructor
	 */
	SinOscillator();
	
	/**
	 SinOscillator destructor
	 */
	virtual ~SinOscillator();
	
    /**
	 function that provides the execution of the waveshape
     @param currentPhase takes the current phase of the oscillator and uses this as a basis to render the wave
     @return rendered waveshape as a sine wave
	 */
	virtual float renderWaveShape (const float currentPhase) override;
	
private:

};

#endif //H_SINOSCILLATOR
